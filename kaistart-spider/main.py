import requests
import json
import hashlib
from datetime import datetime


def get_origin_list(page):
	params = 'page=' + str(page) + '&pagesize=100&markerid=2D0C39ED14BDFA3DE050840AF24219CC&progress=&sort=&client=web'
	url = 'http://www.kaistart.com/api/project/list?' + params
	crypt_str = 'GEThttp://120.55.197.143:8080/crowdfundingservice2/project/list?' + params + '-1www.kaistart.com'
	headers = {'sign': hashlib.md5(crypt_str.encode()).hexdigest()}
	response = requests.get(url, headers=headers)
	result = json.loads(response.text)
	return result['result']
	# print(result['result'][0])


def write_to_file(origin_list):
	result = ['链接\t标题\t发起时间\t结束时间\t目标金额\t认筹总额\t认购人数']
	for item in origin_list:
		result.append('http://www.kaistart.com/project/detail/id/' + item['id'] + '\t' + item['name'] + '\t' + item['startDate'] + '\t' + item['endDate'] + '\t' + str(item['funds']) + '\t' + str(item['total']) + '\t' + str(item['supportNum']))
	result_str = '\n'.join(result)
	# print(result_str)

	date_str = datetime.now().strftime('%Y-%m-%d')
	with open('/Users/Zzy/Desktop/kaistart-' + date_str + '.csv', 'wt') as f:
		f.write(result_str)


def main():
	origin_list = []
	for page in range(1, 1000):
		page_list = get_origin_list(page)
		if not page_list:
			break
		origin_list.extend(page_list)
	write_to_file(origin_list)


if __name__ == '__main__':
	main()
