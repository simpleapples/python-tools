import requests
import argparse
import uuid
from bs4 import BeautifulSoup


def get_ins_large_image(url):
    headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'}
    response = requests.get(url, headers=headers)
    html = response.text
    soup = BeautifulSoup(html, 'html.parser')
    image = soup.find('meta', {'property': 'og:image'})
    return image.get('content')


def download_file(url, path):
    response = requests.get(url)
    file_name = str(uuid.uuid1()) + '.jpg'
    with open(path + '/' + file_name, 'wb') as file:
        file.write(response.content)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('url')
    parser.add_argument('path')
    args = parser.parse_args()
    url = args.url
    path = args.path
    large_image_url = get_ins_large_image(url)
    download_file(large_image_url, path)


if __name__ == '__main__':
    main()